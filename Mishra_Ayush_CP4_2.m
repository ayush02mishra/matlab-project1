%-----------        Parameters
LoadCase = 7;
T = 100;
lx = .1;
ly = .1;
k = 200;
%------------   P Load plotting Parameters
Pmag = .1;
Pclearance = .1;
PlotLoad = false;

FMag = 1000;            %   Force Magnification
gridLengthX = 12;
gridLengthY = 12;
gridNodes = gridLengthX * gridLengthY;
gridA = zeros(gridLengthX,gridLengthY);
J = sym([1/lx 0; 0 1/ly]);
PLoad = zeros(gridLengthX,gridLengthY);

%----------------  Logic
syms ex ey;


psi1 = (1-ex).*(1-ey);
psi2 = (ex).*(1-ey);
psi4 = (1-ex).*(ey);
psi3 = (ex).*(ey);

psi = [psi1 psi2 psi3 psi4];
psipsiT = psi.'*psi;
delU = jacobian(psi,[ex ey]);
delUT = delU.';

Beta = J*delUT;


KGlobal = int(int((Beta.')*(Beta).*lx*ly*T + psipsiT*k,ex,0,1),ey,0,1);
B = zeros(4,gridNodes);
FMembrane = zeros(gridNodes,1);
KMembrane= zeros(gridNodes,gridNodes);
d = zeros(1,gridNodes);
for I = 1:gridNodes-gridLengthX-1;
    
    switch(LoadCase)
        case 1
            p = 1;
        case 2
            p = I*lx*I*lx +I*ly*I*ly;
        case 3
            p = I*lx + I*ly;
        case 4
            p = sin(2*pi*mod(I,gridLengthX)/gridLengthX);
        case 5
            p = sin(2*pi*mod(I,gridLengthX)/gridLengthX)*sin(2*pi*mod(floor(I/gridLengthX)+1,gridLengthY)/gridLengthY);
        case 6
            if ((abs((floor(I/gridLengthX)+1)-gridLengthY/2))<1.5 && abs(mod(I,gridLengthX)-gridLengthX/2)<1.5);
                p = 1;
            else
                p = 0;
            end
        case 7
            if ((abs((floor(I/gridLengthX)+1)-floor(gridLengthY/2)))<3 && abs(mod(I,gridLengthX)-floor(gridLengthX/2))<3 && abs((floor(I/gridLengthX)+1)-floor(gridLengthY/2)))>0 && abs(mod(I,gridLengthX)-floor(gridLengthX/2))>0;
                p = 1;
            else
                p = 0;
            end
    end
    
    
    if (mod(I,gridLengthX)~=0)
        PLoad(floor(I/gridLengthX)+1,mod(I,gridLengthX)) = p*Pmag+Pclearance;
    else
        PLoad(floor(I/gridLengthX)+1,6) = p*Pmag+Pclearance;
    end
    B = zeros(4,gridNodes);
    B(1,I) = 1;
    B(2,I+1) = 1;
    B(3,gridLengthX + I + 1) = 1;
    B(4,gridLengthX + I) = 1;
    BTK = (B.')*KGlobal;
    BTKB = BTK*B;
    KMembrane = KMembrane + BTKB;
    f = int(int(p.*(psi.')*lx*ly*FMag,ex,0,1),ey,0,1);
    BTf = (B.')*f;
    FMembrane = FMembrane + BTf;
end

%-------------------        Impose boundary condition
for I = 1:gridNodes;
    if (0==floor(I/gridLengthX) || (gridLengthY-1)==floor(I/gridLengthX) || 1== mod(I,gridLengthX) || 0== mod(I,gridLengthX) )
        KMembrane(I,:) = 0;
        KMembrane(:,I) = 0;
        KMembrane(I,I) = 1;
        FMembrane(I) = 0;
    end
end
a = linsolve(KMembrane,FMembrane);

a(isnan(a)) = 0;
a(isinf(a)) = 0;
for i = 1:gridLengthX;
    for j = 1:gridLengthY;
        gridA(i,j) = a((i-1)*gridLengthX + j);
    end
end

surf(-gridA);
hold on ;
if (PlotLoad == true)
    surf (PLoad);
end